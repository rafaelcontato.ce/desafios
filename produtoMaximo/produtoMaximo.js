let arr = [-6, 4, -5, 8, -10, 0, 8];

const multiplicador = array => {
    arrayNegativo = bubbleSort(array.filter((valor) => valor < 0));
    arrayPositivo = bubbleSort(array.filter((valor) => valor > 0));

    if(array.length % 2 !== 0)
        arrayNegativo.splice(arrayNegativo.length -1);

    array = arrayNegativo.concat(arrayPositivo);

    return [array, array.reduce((total, elemento) => total *= elemento)]
}

const bubbleSort = (array) => {
    let trocado;
    do {
        trocado = false;
        for (let i = 0; i < array.length; i++) {
            if (array[i] > array[i + 1]) {
                let tmp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = tmp;
                trocado = true;
            }
        }
    } while (trocado);
    return array;
};

console.log(multiplicador(arr));