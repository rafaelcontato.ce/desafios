import { useState } from "react";

export default function App() {
  const [array, setArray] = useState([]);
  const [cotacao, setCotacao] = useState('');
  const [editar, settEditar] = useState(true);
  function adicionaCotacao(e) {
    e.preventDefault();

    if (cotacao)
      setArray(arrayOld => [...arrayOld, cotacao])

    setCotacao('');
  }

  function removerCotacao(elemento) {
    setArray(array.filter((valor) => valor !== elemento));
    setCotacao('');
  }

  function alterarCotacao(elemento, index) {
    setArray(array.map((valor, ind) => {
      if (index === ind)
        valor = elemento
    }));
  }


  return (
    <div className="App">
      <form onSubmit={adicionaCotacao}>
        <input
          value={cotacao}
          onChange={(e) => setCotacao(e.target.value)}
          type="text"
          placeholder="Digite o nome da cotacao"
        />
        <button type="submit"> Adicionar cotacao </button>
        <button onClick={() => settEditar(editar ? false : true)}>
          Editar
        </button>
      </form>

      <div>
        <ul>
          {array.map((elemento, index) => {
            return (
              <li key={index}>
                <input
                  value={elemento}
                  onChange={() => alterarCotacao(elemento, index)}
                  disabled={editar}
                  type="text"
                />
                <button onClick={() => removerCotacao(elemento)}>
                  Remover
                </button>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  );
}